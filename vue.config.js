module.exports = {
  devServer: {
    host: "0.0.0.0",
    port: 3000, // CHANGE YOUR PORT HERE!
    https: true,
    hotOnly: false,
    // proxy: "https://localhost:8080"
    proxy: {
      "/api": {
        target: "https://localhost:8080",
        ws: true,
        changeOrigin: true
      }
    }
  }
};
