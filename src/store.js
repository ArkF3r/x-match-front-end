import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    logged: Boolean
  },
  getters: {
    isLogged(state) {
      return state.logged;
    }
  },
  mutations: {
    CHANGE_LOGGED(state, status) {
      state.logged = status;
    }
  },
  actions: {
    changeLogged(context, status) {
      console.log("changeLogged");
      context.commit("CHANGE_LOGGED", status);
    }
  }
});
