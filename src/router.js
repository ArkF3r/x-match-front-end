import Vue from "vue";
import axios from "axios";
import Router from "vue-router";
import Home from "@/views/Home.vue";
import Login from "@/views/Login.vue";
import Registro from "@/views/Registro.vue";
import Stats from "@/views/Stats.vue";
Vue.use(Router);

//export default
let router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/Login",
      name: "Login",
      component: Login,
      meta: {
        guest: true
      }
    },
    {
      path: "/Registro",
      name: "Registro",
      component: Registro,
      meta: {
        guest: true
      }
    },
    {
      path: "/Stats",
      name: "Stats",
      component: Stats,
      meta: {
        requiresAuth: true
      }
    }
  ]
});

router.beforeEach(async (to, from, next) => {
  var logged = await checkSession();
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // if (localStorage.getItem("logged") == "false") {
    if (!logged) {
      next({
        path: "/login",
        params: { nextUrl: to.fullPath }
      });
    } else {
      next();
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    if (!logged) {
      next();
    } else {
      next({ name: "Stats" });
    }
  } else {
    next();
  }
});

function checkSession() {
  return new Promise(resolve => {
    axios
      .get("/api/Login")
      .then(response => {
        var { data } = response;
        resolve(data.status);
      })
      .catch(() => {
        resolve(false);
      });
  });
}

export default router;
